FROM python:3.5
MAINTAINER Markus Lippus <markus.lippus@mindtitan.com>


COPY . /app
WORKDIR /app

RUN pip install -r requirements.txt

CMD ["./runscript.sh"]
EXPOSE 8888

