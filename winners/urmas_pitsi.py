#import numpy as np
#import pandas as pd
#from sklearn.model_selection import train_test_split
#import xgboost as xgblib
#import gc
#
#probs = pd.read_parquet('../data/probs_interm.parquet')
#norms = pd.read_parquet('../data/norms_interm.parquet')
#
#probs_day = probs.groupby('ident').sum().drop('probs', axis=1)
#norms_day = norms.groupby('ident').sum().drop('probs', axis=1)
#
## load test data
#test = pd.read_parquet('../data/test_interm.parquet')
#test_day = test.groupby('ident').sum()
#
## find common columns
#common_cols = list(set(test_day.columns).intersection(set(norms_day.columns)).intersection(set(probs_day.columns)))
#
#norms_day = norms_day[common_cols]
#probs_day = probs_day[common_cols]
#test_day = test_day[common_cols]
#
#feature_names = list(probs_day.columns)
#feature_names = [x.replace('[', '_') for x in feature_names]
#
## to numpy arrays
#X_full = np.concatenate([probs_day.values, norms_day.values])
#y_full = np.concatenate([probs_day.shape[0] * [1], norms_day.shape[0] * [0]])
#X_full_test = test_day.values
#
#del probs, norms, probs_day, norms_day, test
#gc.collect()
#
#######################################################################
## train
#######################################################################
#X_train, X_test, y_train, y_test = train_test_split(X_full, y_full, test_size=0.1, random_state=0)
#
#params = {
#    'silent': 1, 
#    'eval_metric': 'auc', 
#    'verbose_eval': True, 
#    'seed': 0, 
#    'objective': 'binary:logistic', 
#    'booster': 'gbtree', 
#    'tree_method': 'exact', 
#    'min_child_weight': 1.12,
#    'colsample_bytree': 0.997,
#    'max_depth': 6,
#    'subsample': 0.5055,
#    'gamma': 3.9382,
#    'alpha': 0.8776,
#    'eta': 0.2354
#    }
#
################################################################33
## xgb classifier
################################################################33
#
#clf = xgblib.XGBClassifier(
#        max_depth = params['max_depth'],
#        learning_rate = params['eta'],
#        gamma = params['gamma'],
#        min_child_weight = params['min_child_weight'],
#        subsample = params['subsample'],
#        colsample_bytree = params['colsample_bytree'],
#        reg_alpha = params['alpha'],
#        )
#
#clf.fit(X_train, y_train, eval_metric='auc', verbose=False)
#pred_test = clf.predict(X_full_test)
#
################################################################33
## submit
################################################################33
#
#submission = pd.DataFrame()
#submission['customerID'] = test_day.index
#submission['label'] = pred_test
#submission.to_csv('submission.csv', index=False)
