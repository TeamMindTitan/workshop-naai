import pandas as pd
import datetime as dt
from collections import Counter
from dateutil.relativedelta import relativedelta

def make_stats(df):
    """Takes as input a dataframe """
    stats = []
    cols_of_interest = ['event_result', 'cause_code', 'sub_cause_code',
                       'mecontext', 'event_id']
    for col in cols_of_interest:
        desc = df[col].value_counts()
        desc.index = desc.index.map(lambda x: '{}_{}'.format(col, x))
        stats.append(desc)

    return pd.concat(stats)

def count_unique_values(group):
    categorical_columns = ['cause_code', 'event_id', 'event_result', 'mecontext', 'sub_cause_code']
    return pd.Series({column: len(set(group[column].values)) for column in categorical_columns})

def count_total_values(group):
    categorical_columns = ['cause_code', 'event_id', 'event_result', 'mecontext', 'sub_cause_code']
    return pd.Series({column: len(set(group[column].values)) for column in categorical_columns})

def plot_hmap(src_df, ident, unique=True):
    one_number = src_df.loc[src_df.ident==ident]
    one_number = one_number.loc[one_number.start.dt.date == Counter(one_number.start.dt.date).most_common()[0][0]]
    min_time = one_number.start.min()
    max_time = one_number.start.max()
    init_row = pd.Series({column: 0 for column in one_number.columns})
    init_row.loc['start'] = dt.datetime(min_time.year, min_time.month, min_time.day, 0, 0, 0)

    end_row = pd.Series({column: 0 for column in one_number.columns})
    end_row.loc['start'] = dt.datetime(max_time.year, max_time.month, max_time.day, 0, 0, 0) + relativedelta(days=1)

    one_number = one_number.append(init_row, ignore_index=True)
    one_number = one_number.append(end_row, ignore_index=True)
    
    if unique:
        hmap = one_number.set_index('start').resample('30Min').apply(count_unique_values).dropna(axis=1)
    else:
        hmap = one_number.set_index('start').resample('30Min').apply(count_total_values).dropna(axis=1)
        
    return hmap    

def timerange(start, end, step):
    while start <= end:
        yield start.time()
        start += step

def gen_day_splits(minutes):
    start = dt.datetime.strptime('201803080000', '%Y%m%d%H%M')
    end = dt.datetime.strptime('201803090000', '%Y%m%d%H%M')
    step = dt.timedelta(minutes=minutes)
    splits = [x for x in timerange(start, end, step)]
    return splits