# Introduction

This repository holds the code and data for the workshop held at North Star AI by the collaborative effort of MindTitan and Elisa.  
In this workshop we give a brief example of what cellular data network looks like, explore it a bit and build to models to identify from the logs people who are having issues with their cellular connection.  

In connection with this workshop there is also a [competition held by Elisa](http://www.elisa.ee/ai-challenge). The data for the competition is contained in this repository at `/data/competition_data.parquet`  
It looks similar to the workshop data in most aspects other than that the labels have been removed and data has been shuffled.  

# Instructions

This workshop project can be run as a container or in the host environment, whichever you like best.  
The container option is more fool-proof, but unless you downloaded the pre-built docker image from the competition page, it's going to take a while. Also you don't have access to GPU from the docker.  
The image can be downloaded from [here](http://www.elisa.ee/ai-challenge).  
If you plan to build it yourself, do it before the workshop, as the network will be very congested if many people try this at once.


## Running in docker

Before building or launching the docker image, ensure you have a recent version (17.x) docker installed. If not, there are instructions for all platforms [here](https://docs.docker.com/install/). You need the Community Edition(CE) of docker!

### 1.A. Loading the downloaded docker image

**NOTE:** This is the recommended method!  

The downloaded package comes as a zip. Unzip the .tar.gz file to .tar:
```gunzip nsai-workshop.tar.gz```

Import the tar file into docker by running the following command in the directory where you unzipped the tar file:
```sudo docker load --input nsai-workshop.tar```


### 1.B. Building the docker image  

**NOTE:** Use this method if you know what youre doing!

Clone the repository using HTTPS:
```git clone https://gitlab.com/TeamMindTitan/workshop-naai.git```  

In the directory of the cloned repository, run the following command to build the image:  
```sudo docker build . -t nsai-workshop```

This will take a while as it needs to download several docker images and all required python libraries.

### 2. Run the image

Run the following command in any directory:  
```sudo docker run -itp 8888:8888 nsai-workshop```

There is a link in the terminal that you can use to access the notebook.  

You can also just go to `localhost:8888` from your browser, but then you'll have to copy the token from the terminal to access the notebook.

## Running on host

### 1. Install requirements

I suggest you use anaconda for this, although pip install may work as well. Your funeral.  
Clone the repository using HTTPS:
```git clone https://gitlab.com/TeamMindTitan/workshop-naai.git```  

And install the requirements by running:  

```conda install --yes --file requirements.txt```

### 2. Run the notebook

Run the notebook with:  
```jupyter notebook notebooks/Cell\ network\ data\ analysis.ipynb```  

A browser window should open with the notebook.

# What's in this repo

* data/ - data related to the workshop. .parquet files for workshop and competition data and some additional data about the devices.
* notebooks/ - actually a single notebook in which the workshop is contained.
* scripts/ - script for training the neural net separately from the notebook.
* tools/ - misc scripts imported into notebook but kept separately for reasons of cleanliness.


