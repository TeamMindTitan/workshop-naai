import numpy as np
import sys
import keras
import keras.backend as K
from keras.layers.core import Activation
from keras.models import Sequential,load_model
from keras.layers import Dense, Dropout, LSTM, Embedding
from keras.callbacks import ModelCheckpoint
from sklearn.model_selection import train_test_split
from sklearn.utils import resample
from collections import Counter
from keras import backend as K
from sklearn.utils import shuffle


if len(sys.argv) > 1:
    RUN = sys.argv[1]
else:
    print('Add run name as argument.')
    exit()

#############################################
# LOAD DATA
# Must be saved from the notebook beforehand
#############################################

X_full = np.load(open('../data/X_full.npz', 'rb'))
y_full = np.load(open('../data/y_full.npz', 'rb'))


sequence_length = X_full.shape[1]
num_features = X_full.shape[2]

model = Sequential()
model.add(LSTM(
         input_shape=(sequence_length, num_features),
         units=40,
         return_sequences=True))
# model.add(Dropout(0.2))
model.add(LSTM(
         units=20,
         return_sequences=True))
# model.add(Dropout(0.2))
model.add(LSTM(
          units=20,
          return_sequences=False))
model.add(Dropout(0.2))
model.add(Dense(units=1, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])




X_use, y_use = X_full, y_full

#########################################
# RESAMPLE
# Use subsample if resources are limited
#########################################

# X_use, y_use = resample(X_full, y_full, n_samples=100000, replace=False)

X_train, X_test, y_train, y_test = train_test_split(X_use, y_use)

X_full, y_full = shuffle(X_full, y_full)


counts = Counter(y_use.reshape(len(y_use)))
print('Class distribution: 0 - {}, 1 - {}\nBaseline acc: {}'.format(counts[0], counts[1], counts[0]/(counts[0] + counts[1])))

tb_cb = keras.callbacks.TensorBoard(log_dir='./Graph/{}'.format(RUN),
                                         histogram_freq=0,
                                         write_graph=True,
                                         write_images=True)

es_cb = keras.callbacks.EarlyStopping(
                              monitor='val_loss',
                              min_delta=0,
                              patience=20,
                              verbose=5,
                              mode='min')

model_checkpoint = "../data/model1.h5"
cp_cb = keras.callbacks.ModelCheckpoint(
                           model_checkpoint,
                           monitor='val_loss',
                           save_best_only=True, 
                           mode='min', 
                           verbose=0)


if __name__ == '__main__':
    try:


        model.fit(X_full,
                  y_full,
                  epochs=500,
                  batch_size=4096,
                  validation_split=0.05,
                  verbose=2,
                  callbacks = [tb_cb, es_cb, cp_cb]
                 )

    except KeyboardInterrupt:
        K.clear_session()
        sys.exit()